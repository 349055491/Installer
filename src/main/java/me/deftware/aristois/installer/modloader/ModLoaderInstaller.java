package me.deftware.aristois.installer.modloader;

import me.deftware.aristois.installer.utils.VersionData;

/**
 * @author Deftware
 */
public abstract class ModLoaderInstaller {

	public abstract String install(VersionData data, String rootDir);

}
